//TODO: make it compiled, sothat the parser dependency is in the same file.
function Absurdictionary(){
    var dictionary={};
    function appendWords(list){
        //TODO: failsafe this
        for(var word in list){
            dictionary[word]=list[word];
        }
    }
    function phraseToWords(phrase){
        console.log("phraseToWords",phrase);

        try{
            var parsed=parser.parse(phrase.toLowerCase());
            // for(var n in parsed){
            //     item=parsed[n];
            // }
        }catch(e){
            console.log("parse error",e);
        }
        return parsed;
    }
    function wordsToPhrase(wordsArray){
        //TODO, reverse of phraseToWords
    }
    function findDefinitionFor(word){
        console.log("findDEfinitionFor",word);
        word=word.replace("_","_").trim().toLowerCase();
        var subword=false;//whether the word was found in the branches
        var foundDefinition=dictionary[word];
        if(foundDefinition && foundDefinition.proxy) foundDefinition=dictionary[foundDefinition.proxy];
        
        if(!foundDefinition){
            //find in the fields, in case the declination's parentheses have been forgotten
            foundDefinition=(function(){
                console.log("word not found, looking into the definition branches");
                if(!foundDefinition){
                    for(var iWord in dictionary){
                        // console.log(iWord);
                        for(var iSubword in dictionary[iWord]){
                            // console.log(iWord,"->",iSubword==word,word+iSubword);
                            if(iSubword==word||iWord+iSubword==word){
                                subword=iSubword;
                                return dictionary[iWord];
                            }
                        }
                    }
                }
                return false;
            })();
        }

        //find local dictionary
        if(!foundDefinition){
            console.log("'"+word+"' not found in dictionary");
            //if not found, find through ajax

            //if not, suggest adding it.
        }
        return {foundDefinition,subword};
    }
    this.appendWords=appendWords;
    this.phraseToWords=phraseToWords;
    this.wordsToPhrase=wordsToPhrase;
    this.findDefinitionFor=findDefinitionFor;
}