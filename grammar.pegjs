//example: fish begin(_verb) colo(u)rs creature(s) swim(ming) under the sea[water]

/*
example items from their appearance in phrase:

fish -> { "word": "fish " }
creature(s) -> {
    "word": "creature",
    "displayAs": "creatures",
    "addition": "s",
    "post": ""
}
colo(u)rs -> {
    "word": "colors",
    "displayAs": "colours",
    "addition": "u",
    "post": "rs"
}
sea[water] -> {
    "word": "sea",
    "displayAs": "water"
}
begin(_verb) -> {
    "word": "begin_verb",
    "displayAs": "begin",
}
begin(_noun)(ning) -> {
    addition: "ning",
    displayAs: "beginning",
    post: "",
    word: "begin_noun",
}
*/

Line
	= Factor +

Factor
    = _ word:Word_Punct addition:Parenthesized post:(Word_Punct)* _ {
        //flattens to string
        addition+="";
        post+="";
    	var displayAs = word+addition+post;
        word+=post;
    	return {word,displayAs,addition,post}
    }
    / _ word:Word_Punct disambiguation:Antiparenthesized addition:Parenthesized post:(Word_Punct)*_ {
        //flattens to string
        post+="";
        addition+="";
    	var displayAs = word+addition+post;
        word = word+"_"+disambiguation;
        word+=post;
        disambiguation+="";

    	return {word,displayAs,addition,post}
    }
    / _ word:Word_Punct disambiguation:Antiparenthesized _ {
    	var displayAs = word;
        word = word+"_"+disambiguation;
        disambiguation+="";
    	return {word,displayAs}
    }
    / _ word:Word_Punct replacement:Bracketized _ {
    	var displayAs = replacement+"";
    	return {word,displayAs} 
    }
    / _ Word_Punct _ { 
    	return {word:text()} 
    }

Word_Punct
    = Word { return text() }
    / Punctuation{ return text() }

Word "word"
    = [a-zA-Z\_]+("'s")*("'es")* { return text(); }

Punctuation "punctuation"
    = [\'\.\,\;\:\-]+ { return text(); }

Parenthesized "term in parenthesis"
	= "(" text:([\t ]/ Word)+")" { return text; }
Antiparenthesized
	= "(" _ "_" text:([\t ]/ Word)+")" { return text; }
Bracketized "term in square brackets"
	= "[" text:([\t ]/ Word)+"]" { return text; }

_ "whitespace"
    = [ \t\n\r]*

              