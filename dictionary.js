//index is potential words to be sought, contains "Word"s.

/**
 * rules of definition:
 * 
 * Each word has to be defined in such way that the definition could replace the word and work the same grammatically.
 * this means that definitions generally wont' start with "a" or "the", and so. Also, a definition rarely states their 
 * relation to something. a definition for love would not be "having fondness for something" but rather "having fondness for"
 * because that "something" is expected to be on the containing phrase
 * 
 * I am considering only three types: nouns, verbs and adjectives. Each with different declined expressions:
 * nouns contain expressions in comparative forms like: the adjective, the "more" version, and the "most" version.
 * verbs contain with the normal form, with "s" ending, and with the "ing" ending
 * nouns contain singular and plural.
 * 
 * Sometimes a verb, and noun have the same word. In a definition you can choose to which version it will expand by
 * using a proxy. A proxy is a word that expands to a different word than the word which is displayed, this way you can
 * have a fly_verb and fly as noun. Whenever you want to use fly as a verb, you would use fly_verb[fly]
 * 
 * 
 */
var dictionary = {
	be: {
		be: "take place",
		ing: "taking place",
		was: "existence have[had] take(n) place",
	},
	bind: {
		bind: "glue",
		bound: "glue(d)",
		binding: "glue[gluing]",
	},
	book: {
		book: "pile of page(s) that are bind[bound] in such way that it is possible to observe each individual(ly)",
	},
	column: {
		column: "physical formation with a predominant axis",
	},
	conceptualize: {
		conceptualize: "take an incomplete and virtual presence in the mind",
		conceptualizing: "take[taking] an incomplete and virtual presence in the mind",
		conceptualized: "take[took] an incomplete and virtual presence in the mind",
	},
	definition: {
		definition: "explanation by means of other words,",
	},
	dictionary: {
		dictionary: "book, or its model, whose functionality is to provide the definition of word(s)",
		dictionaries: "book(s), or their model, whose functionality is to provide the definition of word(s)"//replacem.
	},
	different: {
		different: "way that is not the same",
	},
	entity: {
		entity: "instance of",
		entities: "instance(s) of",
	},
	film: {
		film: "thin layer of material",
		films: "thin layer(s) of a material",
	},
	form: {
		form: "perceptible shape",
		forms: "perceptible shape(s)",
	},
	form_verb: {
		form: "give form",
		formed: "given form",
		forms: "gives form",
	},
	functionality: {
		functionality: "function for which it is instrumental",
	},
	glue: {
		proxy: "glue_verb",
	},
	glue_verb: {
		glue: "couple so that they become a new entity together",
		glues: "couple(s) so that they become(s) a new entity together",
		glued: "couple(d) so that they become[became] a new entity together",
	},
	help: {
		help: "make it easier",
		helps: "makes it easier",
	},
	instance: {
		instance: "emergence of be(ing)",
		instances: "emergences of be(ing)",
	},
	material: {
		material: "perceivable system of chemical structure(s)",
		materials: "perceivable system(s) of chemical structure(s)",
	},
	mind: {
		mind: "experience of internal self that is perceive(d) by human consciousness",
		minds: "the experience(s) of internal self[selves] that are perceive(d) by different human consciousness",
	},
	model: {
		model: "representation that helps to conceptualize the represented reality",
	},
	page: {
		page: "film that is manufacture(d) out of tree fiber(s)",
		pages: "film(s) that are manufacture(d) out of tree fiber(s)",
	},
	pile: {
		pile: "column form_verb[formed] by repetitive superimposition",
	},
	possible: {
		possible: "can come to be(ing)",
	},
	provide: {
		proxy: "provide_verb",
	},
	provide_verb: {
		provide: "give access to",
	},
	represent_verb: {
		represent: "provide a representation",
		represents: "provide(s) a representation",
		representing: "provide[providing] a representation",
	},
	representation: {
		representation: "presentation in a different form than the original",
		representations: "presentation(s) done in different form(s) than the original",
	},
	word: {
		word: "sequence of phoneme(s) or their write_verb[written] representation, to which an agre(ed) meaning has been assign(ed)",
		words: "sequence(s) of phoneme(s) or their write_verb[written] representation, to which an agre(ed) meaning has been assign(ed)",
	},
	write_verb: {
		write: "trace meaningful shapes in",
		wrote: "traced meaningful shapes in",
		written: "to which meaningful traces have been traced",
	},
	absurd_adj: {
		absurd: "failing to have_verb meaning",
		more_absurd: "fails more on having_verb meaning",
		most_absurd: "most failed to have_verb meaning",
		absurdness: "failingness to have_verb meaning",
		absurdly: "failing in meaning"
	},
	understandable_adj: {
		understandable: "posibble to be understood",
		more_understandable: "more possible to be understood",
		most_understandable: "most possible to be understood",
		understandability: "possibility to be understood"
	},
	thing: {
		thing: "entity",
		things: "entities"
	},
	concept: {
		concept: "social or cognitive system that is built upon other(s) concepts",
		concepts: "social or cognitive systems that are built upon other(s) concepts"
	},
	meaning: {
		meaning: "relation with the wider conceptual frame",
		meanings: "relations with wider conceptual frames"
	},
	clock: {
		clock: "tool used to measure time",
		clocks: "tools used to measure time"
	},
	time: {
		time: "variable that spans naturally and constantly in the same direction",
		times: "variables that spans naturally and constantly in the same direction"
	},
	measure_verb: {
		measure: "quantificate in standarized numbers, the span of the variable",
		measuring: "quantificating in standarized numbers, the span of the variable",
		measures: "quantificates in standarized numbers, the span of the variable",
		measured: "quantificated in standarized numbers, the span of the variable"
	},
	test_verb: {
		test: "test",
		testing: "testing",
		tests: "tests",
		tested: "tested"
	},
	intend_verb: {
		intend: "assign_verb the use",
		intending: "assigning_verb the use",
		intends: "assigns_verb the use",
		intended: "assigned_verb the use"
	},
	agree_verb: {
		agree: "have an answer in common",
		agreeing: "having an answer in common",
		agrees: "has an answer in common",
		agreed: "had an answer in common"
	},
	conceptual_adj: {
		conceptual: "concerns concepts",
		more_conceptual: "more concept concerning",
		most_conceptual: "concerns most the concepts",
		conceptualiness: "concept concerning"
	},
	relation: {
		relation: "bond between some subjects",
		relations: "bonds between some subjects"
	},
	some: {
		some: "two or more"
	},
	frame: {
		frame: "structure that embrace",
		frames: "structures that embrace"
	},
	fail_verb: {
		fail: "get any of the least wanted(adj) outcomes from the action",
		failing: "getting any of the least wanted(adj) outcomes from the action",
		fails: "gets any of the least wanted(adj) outcomes from the action",
		failed: "got any of the least wanted(adj) outcomes from the action"
	},
	have_verb: {
		have: "be attributed to itself",
		having: "being attributed to itself",
		has: "is attributed to itself",
		had: "was attributed to itself"
	},
	system: {
		system: "structure of entities that conform a new, emerging entity",
		systems: "structures of entities that conform a new, emerging entity"
	},
	sequence: {
		sequence: "serial ordered system",
		sequences: "serial ordered systems"
	},
	cognitive_adj: {
		cognitive: "regards the human brain",
		more_cognitive: "regards more the human brain",
		most_cognitive: "regards the most the human brain"
	},
	build_verb: {
		build: "join parts together towards making a system",
		building: "joining parts together towards making a system",
		builds: "joins parts together towards making a system",
		built: "joined parts together towards making a system"
	},
	epiphany: {
		epiphany: "manifestation of a divine nature",
		epiphanies: "manifestations of a divine nature"
	},
	love_verb: {
		love: "feel an affection",
		loving: "feeling an affection",
		loves: "feels an affection",
		loved: "felt an affection"
	},
	attain_verb: {
		attain: "make real",
		attaining: "making real",
		attains: "makes real",
		attained: "made real"
	},
	variable: {
		variable: "chunk of information concerning a characteristic of something",
		variables: "chunks of information concerning a characteristic of something"
	},
	structure: {
		structure: "arrangement that makes a system up from a conglomerate of elements",
		structures: "arrangements that make a system up from a conglomerate of elements"
	},
	outcome: {
		outcome: "effect of a process",
		outcomes: "effects of a process"
	},
	explain_verb: {
		explain: "put in understandable words",
		explaining: "putting in understandable words",
		explains: "puts in understandable words",
		explained: "putted in understandable words"
	},
	agreed_adj: {
		agreed: "equally understood within many minds",
		more_agreed: "more equally understood within many minds"
	},
	understand_verb: {
		understand: "have any idea of how the reality of a concept may really be",
		understanding: "having any idea of how the reality of a concept may really be",
		understands: "has any idea of how the reality of a concept may really be",
		understood: "had any idea of how the reality of a concept may really be"
	},
	cat: {
		cat: "an instance of a particular kind of feline",
		cats: "some instances of a particular kind of feline"
	},
	wide_adj: {
		wide: "large in span",
		wider: "larger in span",
		widest: "largest in span",
		wideness: "span length"
	},
	get_verb: {
		get: "attain the state of having from the state of not having",
		getting: "attaining the state of having from the state of not having",
		gets: "attains the state of having from the state of not having",
		got: "attained the state of having from the state of not having"
	},
	brain: {
		brain: "organ of the nervous_system that fulfills the function of commanding actions or thoughts based on the perception",
		brains: "organs of the nervous_system that fulfills the function of commanding actions or thoughts based on the perception"
	},
	tool: {
		tool: "object that enables or facilitates a living being, the performance of an action",
		tools: "objects that enable or facilitate a living being, the performance of an action"
	},
	object: {
		object: "physical entity",
		objects: "physical entities"
	},
	year: {
		year: "period of time that transcur between 1st of January and the next 1st of January",
		years: "periods of time that transcur between 1st of January and the next 1st of January"
	},
	do_verb: {
		do: "do",
		doing: "doing",
		does: "does",
		did: "did"
	},
	make_verb: {
		make: "give instance to an action or entity",
		making: "giving instance to an action or entity",
		makes: "gives instance to an action or entity",
		made: "gave instance to an action or entity"
	},
	action: {
		action: "performance of a verb",
		actions: "performances of a verb"
	},
	something_pron: {
		something: "an undetermined component of a group",
		somethings: "undetermined components of a group"
	},
	stupid_adj: {
		stupid: "lacks intelligence",
		more_stupid: "lacks intelligence more",
		most_stupd: "most lacking in intelligence",
		supidity: "lacking of intelligence"
	},
	house: {
		house: "human(adj) building that is intended to host people",
		houses: "human buildings that are intended to host people"
	},
	building: {
		building: "artificial artifact that produce a separation between an inner and an outer side",
		buildings: "artificial artifacts that produce a separation between an inner and an outer side"
	},
	artificial_adj: {
		artificial: "made deliberately by an intelligent being",
		more_artificial: "more entirely made deliberately by an intelligent being",
		most_artificial: "most entirely made deliberately by an intelligent being",
		artificialiness: "how much was done by an intelligent being",
		artificially: "deliberately by an intelligent being"
	},
	human: {
		human: "human(adj) living instance",
		humans: "human(adj) living instances"
	},
	human_adj: {
		human: "of an animal specie that is able to be aware of himself by the use of his brain"
	},
	have_auxv: {
		have: "have",
		has: "has"
	},
	oneself: {
		oneself: "one own[own's] self",
		ourselves: "our own[own's] self"
	},
	own_verb: {
		own: "posses abstractly a entity",
		owning: "posessing abstractly a entity",
		owns: "posesses abstractly a entity",
		owned: "posessed abstractly a entity"
	},
	perception: {
		perception: "reality that is generated in the basis of what the brain thinks from what the sensing organs sense_verb",
		perceptions: "realities that are generated in the basis of what the brain thinks from what the sensing organs sense_verb"
	},
	young_adj: {
		young: "recently built",
		younger: "built more recently",
		youngest: "built most recently",
		youngness: "time span from when it was built"
	},
	itself_pron: {
		itself: "his own self"
	},
	himself_pron: {
		himself: "his {own's/own} self"
	},
	affection: {
		affection: "intrinsically human feeling that regards some human other, and is characterized for the need of: being near to the other, making it happy, and knowing that it has a reciprocal feeling",
		affections: "intrinsically human feelings that regard some human other, and is characterized for the need of: being near to the other, making it happy, and knowing that it has a reciprocal feeling"
	},
	feeling: {
		feeling: "perception of an emotion",
		feelings: "perceptions of emotions"
	},
	emotion: {
		emotion: "sensation of an subconscious, brain generated, reality",
		emotions: "sensations of subconscious, brain generated, realities"
	},
	reality: {
		reality: "imaginary and impossibly right unbiased perception of the existence",
		realities: "imaginary and impossibly right unbiased perceptions of the existence"
	},
	affliction: {
		affliction: "perception that something is causing a deep damage",
		afflictions: "perceptions that something is causing a deep damage"
	},
	letter: {
		letter: "graphic that represent a speech sound",
		letters: "graphics that represents speech sounds"
	},
	sound: {
		sound: "oscillating air pressure variation that contains at least one harmonic component inside the audible range",
		sounds: "oscillating air pressure variations that contain at least one harmonic component inside the audible range"
	},
	variation: {
		variation: "change along the time",
		variations: "changes along the time"
	},
	along_prep: {
		along: "within the span of"
	},
	music: {
		music: "artistic sequence of some sounds, that are perceived as being related sequentially",
		musics: "artistic sequences of some sounds, that are perceived as being related sequentially"
	},
	perceive_verb: {
		perceive: "make a perception",
		perceiving: "making a perception",
		perceives: "makes a perception",
		perceived: "made a perception"
	},
	man: {
		man: "male human",
		men: "male humans"
	},
	wage: {
		wage: "pecuniary payment in exchange of time dedication",
		wages: "pecuniary payments in exchange of time dedication"
	},
	virtual_adj: {
		virtual: "perceivable but not real"
	},
	hazardous_adj: {
		hazardous: "damage inducing",
		more_hazardous: "more damage inducing",
		most_hazardous: "induces most the damage",
		hazardousiness: "inducement of damage",
		hazardously: "with inducement of damage"
	},
	woman: {
		woman: "female human",
		women: "female humans"
	},
	real_adj: {
		real: " in accordance to the reality",
		"*": "",
		really: " in accordance to the reality"
	},
	art: {
		art: "men wanted for hazardous journey.low wages, bitter cold, long hours of complete darkness.Safe return doubtful.honour and recognition in event of success",
		arts: "disciplines of art"
	},
	success: {
		success: "most wanted outcome from an action",
		successes: "most wanted outcomes from actions"
	},
	succeed_verb: {
		succeed: "get any of the most wanted outcomes from an action",
		succeeding: "geting any of the most wanted outcomes from an action",
		succeeds: "gets any of the most wanted outcomes from an action",
		succeeded: "got any of the most wanted outcomes from an action"
	},
	horse: {
		horse: "instance of an animal that has odd - toed ungulates, belonging to the family equidae, that is usually domesticated",
		horses: "instances of an animal that has dd - toed ungulates, belonging to the family equidae, that is usually domesticated"
	},
	abstract_adj: {
		abstract: "perceivable only with the mind",
		more_abstract: "perceivable in a higher extent only with the mind",
		most_abstract: "perceivable in the highest extent, only with the mind",
		abstractness: "extent to which something is only perceivable using the mind",
		abstractly: "perceivable only with the mind"
	},
	print_verb: {
		print: "append into the object, a new feature that was potentially possible through an external agent",
		printing: "appending into the object, a new feature that was potentially possible through an external agent",
		prints: "appends into the object, a new feature that was potentially possible through an external agent",
		printed: "appended into the object, a new feature that was potentially possible through an external agent"
	},
	print: {
		print: "support that has been printed",
		prints: "supports that have been printed"
	},
	give_verb: {
		give: "transfer ownership of",
		giving: "transferring ownership of",
		gives: "transfers ownership of",
		gave: "transferred ownership of"
	},
	part: {
		part: "entity within a system",
		parts: "entities within one or more systems"
	},
	sequential_adj: {
		sequential: " in serial ordered fashion",
		more_sequential: "sorted more in serial fashion",
		most_sequential: "most sorted in a serial fashion",
		sequentialiness: "amount of serially sorted parts",
		sequentially: " in a serial ordered fashion"
	},
	audible_adj: {
		audible: "can be perceived through hearing",
		more_audible: "can be perceived more through hearing",
		most_audible: "most perceivable through hearing",
		audibliness: "with capacity to be perceived through hearing",
		audiblely: "perceivable through hearing"
	},
	oscillate_verb: {
		oscillate: "vary periodically and repetitively",
		oscillating: "variating repetitively and periodically",
		oscillates: "varies periodically and repetitively",
		oscillated: "varied periodically and repetitively"
	},
	repetitive_adj: {
		repetitive: "perceivably built with many identical entities",
		more_repetitive: "more perceivably built with many identical entities",
		most_repetitive: "most perceivably built with many identical entities",
		repetitiviness: "perceivability of being built with many identical entities",
		repetitively: "perceivably built with many identical entities"
	},
	tree: {
		tree: "living system that grows from a seed, is nurtured by soil, and is characterized by growing tree_logically",
		trees: "living systems that grow from a seed, is nurtured by soil, and is characterized by growing tree_logically"
	},
	honour: {
		honour: "socially well valued and recognized achievement or attribute",
		honours: "socially well valued and recognized achievements or attribute"
	},
	january: {
		january: "first month of a year",
		januaries: "some year's first months"
	},
	chunk: {
		chunk: "group of parts within a system, that are delimited artificially",
		chunks: "groups of parts within a system, that are delimited artificially"
	},
	tree_logical_adj: {
		tree_logical: "ordered(adj) mostly in the basis of sequential subordination and bifurcation",
		more_tree_logical: "ordered(adj) more in the basis of sequential subordination and bifurcation",
		most_tree_logical: "most exclusively ordered(adj) in the basis of sequential subordination and bifurcation",
		tree_logicaliness: "how exclusively is ordered(adj) in the basis of sequential subordination and bifurcation",
		tree_logically: "ordered(adj) exclusively in the basis of sequential subordination and bifurcation"
	},
	ordered_adj: {
		ordered: "bind in it's inner elements with relations that grant meaning to it"
	},
	live_verb: {
		live: "have life",
		living: "having life",
		lives: "has life",
		lived: "had life"
	},
	other: {
		other: "different in instance",
		others: "multiple instances that are different in instance "
	},
	near_adj: {
		near: "spaced with a small distance",
		nearer: "spaced with a smaller distance",
		nearest: "spaced with the smallest distance",
		nearness: "smallness of the distance that spaces",
		nearly: "spaced with a small distance"
	},
	contain_verb: {
		contain: "hold inside",
		containing: "holding inside",
		contains: "holds inside",
		contained: "held inside"
	},
	want_verb: {
		want: "believe that having will grant more happiness than not having ",
		wanting: "believing that having will grant more happiness than not having ",
		wants: "believes that having will grant more happiness than not having ",
		wanted: "believed that having will grant more happiness than not having "
	},
	can: {
		can: "to be able"
	},
	user: {
		user: "living creature that uses",
		users: "living creatures that use"
	},
	could: {
		could: "would it be possible that"
	},
	possible_adj: {
		possible: "in accordance with logic and all constraining rules",
		more_possible: "more in accordance with logic and all constraining rules",
		most_possible: "most in accordance with logic and all constraining rules",
		possibility: "fact of being in accordance with logic all its constraining rules",
		possibly: "in accordance with logic and all constraining rules"
	},
	design_verb: {
		design: "designate about all the aspects of",
		designing: "designating about all the aspects of",
		designs: "designates about all the aspects of",
		designed: "designated about all the aspects of"
	},
	designate_verb: {
		designate: "make a decision and communicate its result",
		designating: "making a decision and communicating its result",
		designates: "makes a decision and communicates its result",
		designated: "made a decision and communicated its result"
	},
	fish: {
		fish: "one of the cold - blooded aquatic vertebrates, which most often have fins, gills, and a hydrodynamic body ",
		fishes: "of the cold - blooded aquatic vertebrates, which most often have fins, gills, and a hydrodynamic body "
	},
	intelligence: {
		intelligence: "ability to think",
		intelligences: "abilities to think"
	},
	intelligent_adj: {
		intelligent: "able to think",
		more_intelligent: "better able to think",
		most_intelligent: "best able to think",
		intelligency: "ability to think",
		intelligently: "manifesting ability to think"
	},
	lack_verb: {
		lack: "not have",
		lacking: "out of any",
		lacks: "doesnt have",
		lacked: "didnt have"
	},

	expression: {
		expression: "act of expressing",
		expressions: "acts of expressing"
	},

	express: {
		express: "make the thing's inner nature perceivable",
		expressing: "making the thing's inner nature perceivable",
		expresses: "makes the thing's inner nature perceivable",
		expressed: "made the thing's inner nature perceivable"
	},
	
	// The following ones were written by chat gpt, with a few needed fixes, and might still have problems:

	perfect: {
		perfect: "absolutely complete and flawless",
		perfectly: "absolutely completely and flawlessly",
	},
	unique: {
		unique: "a one-of-a-kind occurrence, unlike anything else",
		uniquely: "in a one-of-a-kind way, unlike any other",
	},
	dead: {
		dead: "no longer alive or functioning",
		deaths: "the cessation of biological life functions",
	},

	function: {
		function: "purpose or use",
		functions: "various purposes or uses",
		functioning: "process of operating or working properly",
		functional: "able to perform its intended purpose,",
		more_functional: "that is able to perform its intended purpose more effectively than something else",
		most_functional: "that is able to perform its intended purpose most effectively,",
	},

	instrumental: {
		instrumental: "which is a tool or means for accomplishing something",
		instrumentally: "in a way that uses tools or means to accomplish something,",
		instrumentality: "quality or state of being instrumental,",
		instrumentalize: "make something into a tool or means for accomplishing a specific goal,",
		instrumentalizes: "make something into tools or means for accomplishing a specific goal,",
	},


	freedom: {
		freedom: "power or right to act, speak, or think as one wants without hindrance or restraint",
		freedoms: "different powers or rights to act, speak, or think as one wants without hindrance or restraint",
	},

	free: {
		free: "able to act, speak, or think as one wants without hindrance or restraint",
	},

	knowledge: {
		knowledge: "facts, information, and skills acquired through experience or education",
		knowledges: "different facts, information, and skills acquired through experience or education",
		knowing: "having facts, information, and skills acquired through experience or education",
	},

	unknowing: {
		unknowing: "not knowledge"
	},

	happiness: {
		happiness: "state of being happy, feeling pleasure or contentment",
		happinesses: "different states of being happy, feeling pleasure or contentment",
	},


	happy: {
		happy: "feeling pleasure or contentment",
	},

	unhappy: {
		unhappy: "not feeling pleasure or contentment"
	},

	trust_verb: {
		trust: "believe firmly in the reliability, truth, or ability of",
		trusts: "different firm beliefs in the reliability, truth, or ability of",
		trusting: "having a firm belief in the reliability, truth, or ability of",
	},

	trusting_adjective: {
		trusting: "having a firm belief in the reliability, truth, or ability of",
		more_trusting: "having a more firm belief in the reliability, truth, or ability of",
		most_trusting: "having the most firm belief in the reliability, truth, or ability of",
	},

	trust_noun: {
		trust: "firm belief in the reliability, truth, or ability of",
		trusts: "different firm beliefs in the reliability, truth, or ability of",
	},

	love: {
		love: "intense feeling of deep affection or fondness",
		loves: "intense feelings of deep affection or fondness for different things",
		lovable: "able to be loved or worthy of love",
		unlovable: "not able to be loved or not worthy of love"
	},

	justice: {
		justice: "quality of being fair and reasonable, especially in the way people are treated or decisions are made",
		justices: "instances of being fair and reasonable, especially in the way people are treated or decisions are made",
		just: "conforming to standards of fairness and reasonableness, especially in the way people are treated or decisions are made",
		unjust: "not conforming to standards of fairness and reasonableness, especially in the way people are treated or decisions are made"
	},
	beauty: {
		beauty: "combination of qualities, such as shape, color, or form, that pleases the senses, especially the sight",
		beauties: "different combinations of qualities that please the senses, especially the sight",
		beautiful: "having qualities that please the senses, especially the sight",
		ugly: "having qualities that displease the senses, especially the sight"
	},
	wisdom: {
		wisdom: "quality of having experience, knowledge, and good judgment",
		wisdoms: "instances of having experience, knowledge, and good judgment",
		wise: "demonstrating experience, knowledge, and good judgment",
		unwise: "not demonstrating experience, knowledge, and good judgment"
	},
	peace: {
		peace: "state of tranquility, calmness, and absence of hostility",
		peaces: "different states of tranquility, calmness, and absence of hostility",
		peaceful: "characterized by tranquility, calmness, and absence of hostility",
		unpeaceful: "characterized by a lack of tranquility, calmness, and absence of hostility"
	},
	hope: {
		hope: "feeling of expectation and desire for a particular thing to happen",
		hopes: "feelings of expectation and desire for different things to happen",
		hopeful: "having feelings of expectation and desire for a particular thing to happen",
		hopeless: "lacking feelings of expectation and desire for a particular thing to happen"
	},
	joy: {
		joy: "feeling of great happiness and delight",
		joys: "feelings of great happiness and delight",
		joyful: "experiencing or demonstrating feelings of great happiness and delight",
		joyless: "lacking feelings of great happiness and delight"
	},
	curiosity: {
		curiosity: "strong desire to know or learn something",
		curiosities: "strong desires to know or learn different things",
	},

	curious: {
		curious: "demonstrating a strong desire to know or learn something",
		more_curious: "demonstrating a stronger desire to know or learn something",
		most_curious: "demonstrating the strongest desire to know or learn something",
	},
	uncurious: {
		uncurious: "lacking a strong desire to know or learn something",
	},
	power: {
		power: "ability to do something or act in a particular way, often with great strength or force",
		powers: "abilities to do things or act in particular ways, often with great strength or force",
		powerful: "possessing great ability to do things or act in particular ways, often with great strength or force",
		powerless: "lacking ability to do things or act in particular ways, often with great strength or force"
	},

	courage: {
		courage: "ability to face danger, difficulty, uncertainty, or pain without becoming blocked by fear or discouragement",
	},

	courageous: {
		courageous: "possessing courage",
		more_corageous: "possessing more courage",
		most_corageous: "possessing the most courage",
	},

	uncourageous: {
		uncourageous: "lacking courage",
	},
	cowardice: {
		cowardice: "an inclination toward or demonstration of fear or discouragement in the face of danger, difficulty, uncertainty, or pain",
	},

	compassion: {
		compassion: "a feeling of deep sympathy and sorrow for another who is stricken by misfortune, accompanied by a strong desire to alleviate the suffering",
		compassions: "feelings of deep sympathy and sorrow for others who are stricken by misfortune, accompanied by a strong desire to alleviate the suffering",
		compassionate: "possessing compassion",
	},

	connection: {
		connection: "sense of being linked or related to someone or something else",
		connections: "senses of being linked or related to different people or things",
		connected: "having a sense of being linked or related to someone or something else",
		disconnected: "lacking a sense of being linked or related to someone or something else"
	},

	stroll: {
		stroll: "leisurely walk taken for pleasure, often with someone else",
		strolling: "walking leisurely for pleasure, often in a meandering or circuitous way",
		strolls: "multiple leisurely walks taken for pleasure, often with someone else and in different locations",
	},
	stroller: {
		stroller: "someone who strolls"
	},
	horse: {
		horse: "quadrupedal mammal, typically used for riding, racing, or farm work",
		horses: "multiple quadrupedal mammals, typically used for riding, racing, or farm work",

	},
	dog: {
		dog: "domesticated carnivorous mammal, typically kept as a pet or trained to hunt",
		dogs: "multiple domesticated carnivorous mammals, typically kept as pets or trained to hunt",

	},
	// coauthred with github copilot
	computer: {
		computer: "machine that can be programmed to carry out sequences of arithmetic or logical operations automatically",
		computers: "machines that can be programmed to carry out sequences of arithmetic or logical operations automatically",
	},

	music: {
		music: "art of combining vocal or instrumental sounds to produce beauty of form, harmony, and expression of emotion",
		musics: "arts of combining vocal or instrumental sounds to produce beauty of form, harmony, and expression of emotion",
	},
		
	musical: {
		musical: "possessing or demonstrating the art of combining vocal or instrumental sounds to produce beauty of form, harmony, and expression of emotion",
		more_musical: "possessing or demonstrating more of the art of combining vocal or instrumental sounds to produce beauty of form, harmony, and expression of emotion",
		most_musical: "possessing or demonstrating the most of the art of combining vocal or instrumental sounds to produce beauty of form, harmony, and expression of emotion",
	},

	noise: {
		noise: "sound that is loud, unpleasant, or unexpected",
		noises: "sounds that are loud, unpleasant, or unexpected",
	},

	silent: {
		silent: "lacking sound",
		more_silent: "lacking more sound",
		most_silent: "lacking the most sound",
	},

	access: {
		access: "ability to approach, enter, or use",
		accesses: "abilities to approach, enter, or use",
	},

	accessible: {
		accessible: "able to be approached, entered, or used",
		more_accessible: "able to be approached, entered, or used more",
		most_accessible: "able to be approached, entered, or used the most",
	},

	inaccessible: {
		inaccessible: "not accessible",
		less_accessible: "less accessible",
		least_accessible: "least accessible",
	},

	observe: {
		observe: "perceive or detect with the senses",
		observes: "perceives or detects with the senses",
		observing: "perceiving or detecting with the senses",
	},

	individually: {
		individually: "separately",
	},

	individual: {
		individual: "separate",
		individuals: "separated units of",
	},

	separately: {
		separately: "deferring each unit to a different instance",
	},

	predominant: {
		predominant: "most notable or important",
		more_predominant: "more notable or important",
		most_predominant: "most notable or important",
	},

	friend: {
		friend: "person with whom one has a bond of mutual affection, typically exclusive of sexual or family relations",
		friends: "people with whom one has bonds of mutual affection, typically exclusive of sexual or family relations",
	},
	intrinsic: {
		intrinsic: "in a way that is inherent to",
	},
	intrinsically: {
		intrinsically: "in a way that is inherent to",
	},
	inherent: {
		inherent: "existing in something as a permanent, essential, or characteristic",
	},
	essential: {
		essential: "absolutely necessary",
	},
	absolutely: {
		absolutely: "in a way that otherwise would not be logical",
	},

}